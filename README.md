# Kubernetes Workshop
Lab 06: Using Configmaps and Secrets

---

## Instructions

### Ensure that your environment is clean

 - List existent deployments
```
kubectl get all
```

### Create Configmaps and Secrets from CLI

 - Create a basic Configmaps to store a shared variable
```
kubectl create configmap language --from-literal=LANGUAGE=English
```

 - Create a basic Secret to store a shared API Token
```
kubectl create secret generic apikey --from-literal=API_KEY=123–456
```

 - List existent ConfigMaps 
```
kubectl get configmaps 
```

 - List existent Secrets
```
kubectl get secrets
```

 - Inspect the created configmap
```
kubectl describe configmap language
```

 - Inspect the created secret (note that you can see the bytes only)
```
kubectl describe secret apikey
```

### Create a pod to print the configmap and secret

 - Inspect the pod definition
```
curl https://gitlab.com/sela-aks-workshop/lab-06/raw/master/cli-test-pod.yaml
```

 - Let's create a pod to use those variables
```
kubectl apply -f https://gitlab.com/sela-aks-workshop/lab-06/raw/master/cli-test-pod.yaml
```

 - Let's see the pod output
```
kubectl logs cli-test
```

### Retrieve the secret value

 - Retrieve the secret definition
```
kubectl get secret apikey -o yaml
```

 - Copy the secret value from the previous command output (line 3)
```
apiVersion: v1
data:
  API_KEY: MTIz4oCTNDU2
kind: Secret
metadata:
  creationTimestamp: "2020-03-14T18:10:01Z"
  name: apikey
  namespace: dev
  resourceVersion: "47025"
  selfLink: /api/v1/namespaces/dev/secrets/apikey
  uid: 05a2d8b6-661f-11ea-a70f-42e5872744c1
type: Opaque
```

 - Use the command below to see the value
```
echo MTIz4oCTNDU2 | base64 -d
```

### Create Configmap from YAML manifest and use it as file

 - Let's create a ConfigMap resource (index.html and .css file)
```
kubectl apply -f https://gitlab.com/sela-aks-workshop/lab-06/raw/master/file-configmap.yaml
```

 - Inspect the ConfigMap definition
```
curl https://gitlab.com/sela-aks-workshop/lab-06/raw/master/file-configmap.yaml
```

 - Create a deployment to run nginx and use the ConfigMap files 
```
kubectl apply -f https://gitlab.com/sela-aks-workshop/lab-06/raw/master/file-deployment.yaml
```

 - Inspect the deployment definition
```
curl https://gitlab.com/sela-aks-workshop/lab-06/raw/master/file-deployment.yaml
```

 - Create a service to expose the web server
```
kubectl apply -f https://gitlab.com/sela-aks-workshop/lab-06/raw/master/file-service.yaml
```

 - Inspect the service definition
```
curl https://gitlab.com/sela-aks-workshop/lab-06/raw/master/file-service.yaml
```

 - List existent services
```
kubectl get services
```

 - Wait a bit until the service finalizes to create the Load Balancer. Then browse to the web application
```
http://<service-ip>:80
```

### Update ConfigMaps and access to container pods

 - Let's update the html file in the previous ConfigMap resource
```
kubectl edit configmap demo-app-content
```

 - Update the welcome message:
```
From: Hello from my ConfigMap!
To: Hello from my updated ConfigMap!!!
```

 - List existent pods
```
kubectl get pods
```

 - Access to the pod container
```
kubectl exec -it <pod-id> /bin/sh 
```

 - Inspect the html file content and exit from the container
```
cat /usr/share/nginx/html/index.html
```
```
exit
```

 - Browse to the web application to see the changes:
```
http://<service-ip>:80
```

 - Let's delete the current pod, kubernetes will provide a new one:
```
kubectl delete pod <pod-id>
```

 - Browse to the web application again
```
http://<service-ip>:80
```

### Cleanup

 - List existent resources
```
kubectl get all
```

 - Delete existent resources
```
kubectl delete all --all
```

 - List existent resources
```
kubectl get all
```